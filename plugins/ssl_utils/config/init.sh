#!/bin/sh

PLUGIN_VERSION="0.0.1"
PLUGIN_DESCRIPTION="Get/Set $CLI_MAIN_COMMAND config"
PLUGIN_AUTHORS="Greg"

__CONFIG_KEYS_LIST=""

list_config_from() {
  CONFIG_FILE=$1
  CONFIG_KEY=$2
  if [ -e "$CONFIG_FILE" ]; then
    while read LINE; do
      LINE=`echo $LINE | sed -e 's/#.*$//'`
      if [ ! "x$LINE" = "x" ]; then
        FOUND_CONFIG_KEY=`echo $LINE | sed -e 's/=.*$//'`
        FOUND_CONFIG_VALUE=`echo $LINE | sed -e 's/^.*=//'`
        if [ "x$CONFIG_KEY" = "x" ] || [ "$CONFIG_KEY" = "$FOUND_CONFIG_KEY" ] ; then
          CONFIG_KEY_PRESENT=`echo $__CONFIG_KEYS_LIST | grep ":$FOUND_CONFIG_KEY"`
          if [ "x$CONFIG_KEY_PRESENT" = "x" ] ; then
            __CONFIG_KEYS_LIST=$__CONFIG_KEYS_LIST:$FOUND_CONFIG_KEY
            echo "$FOUND_CONFIG_KEY : $(eval echo \$$FOUND_CONFIG_KEY)"
          fi
        fi
      fi
    done < "$CONFIG_FILE"
  fi
}

list_config() {
  __CONFIG_KEYS_LIST=""
  list_config_from "$HOME/.config/$CLI_MAIN_COMMAND/config"
  list_config_from "$CLI_ROOT_PATH/default_config.sh"
}

get_config() {
  KEY=$1
  __CONFIG_KEYS_LIST=""
  list_config_from "$HOME/.config/$CLI_MAIN_COMMAND/config" "$KEY"
  list_config_from "$CLI_ROOT_PATH/default_config.sh" "$KEY"
}

reset_config() {
  KEY=$1
  CONFIG_KEY_PRESENT=`cat "$CLI_ROOT_PATH/default_config.sh" | grep "^$KEY"`
  if [ "x$CONFIG_KEY_PRESENT" = "x" ] ; then
    >&2 echo "<!!> Invalid config key $KEY"
    exit 1
  fi

  [ -f "$HOME/.config/$CLI_MAIN_COMMAND/config" ] && sed -i -e "/$KEY=.*/d" $HOME/.config/$CLI_MAIN_COMMAND/config
}

set_config() {
  KEY=$1
  VALUE=$2
  CONFIG_KEY_PRESENT=`cat "$CLI_ROOT_PATH/default_config.sh" | grep "^$KEY"`
  if [ "x$CONFIG_KEY_PRESENT" = "x" ] ; then
    >&2 echo "<!!> Invalid config key $KEY"
    exit 1
  fi

  [ -f "$HOME/.config/$CLI_MAIN_COMMAND/config" ] || touch "$HOME/.config/$CLI_MAIN_COMMAND/config"
  CONFIG_KEY_PRESENT=`cat "$HOME/.config/$CLI_MAIN_COMMAND/config" | grep "^$KEY"`
  if [ "x$CONFIG_KEY_PRESENT" = "x" ] ; then
    echo "$KEY=\"$VALUE\"" >> "$HOME/.config/$CLI_MAIN_COMMAND/config"
  else
    sed -i -e "s#$KEY=.*#$KEY=\"$VALUE\"#" "$HOME/.config/$CLI_MAIN_COMMAND/config"
  fi
}

config_install() {
  _PREFIX_SET="0"

  while [ "$1" ] ; do
    case "$1" in
      "--openssl-version")
        shift
        __OPENSSL_VERSION=$1
        shift
        ;;
      "--openssl-prefix")
        shift
        __OPENSSL_PREFIX=$1
        _PREFIX_SET="1"
        shift
        ;;
      "--openssl-configure")
        shift
        __OPENSSL_CONFIGURE="yes"
        ;;
      "--openssl-no-configure")
        shift
        __OPENSSL_CONFIGURE="no"
        ;;
      *)
        _WHAT=$1
        shift
        ;;
    esac
  done

  if [ "$_WHAT" = "openssl" ] ; then
    if [ "$_PREFIX_SET" = "0" ] ; then
      __OPENSSL_PREFIX="$HOME/.cache/ssl_utils/openssl/${__OPENSSL_VERSION}"
    fi
    eval "install_${_WHAT} \"$__OPENSSL_VERSION\" \"$__OPENSSL_CONFIGURE\" \"$__OPENSSL_PREFIX\""
  else
    >&2 echo "<!!> Don't know how to install $_WHAT."
  fi
}

install_openssl() {
  _VERSION=$1
  _CONFIGURE=$2
  _PREFIX=$3

  _OPENSSL_EXE="${_PREFIX}/bin/openssl"
  _TMP=$(mktemp -d)
  _OPENSSL_ARCHIVE="openssl-${_VERSION}.tar.gz"
  _OPENSSL_PATH="${_TMP}/openssl-${_VERSION}"
  _CURRENT_DIR=$PWD

  printf "<**> Download openssl $_VERSION."
  curl -s "https://www.openssl.org/source/${_OPENSSL_ARCHIVE}" -o "$_TMP/$_OPENSSL_ARCHIVE" 2>&1 1> "$_TMP/openssl-$_VERSION-config.log"
  if [ "$?" = "0" ] ; then
    printf "."
  else
    echo "error!"
    >&2 echo "<!!> Failed to download openssl $_VERSION. See $_TMP/openssl-$_VERSION-config.log"
    exit 1
  fi
  cd "$_TMP"
  tar zxf "$_OPENSSL_ARCHIVE" 2>&1 1>> "$_TMP/openssl-$_VERSION-config.log"
  if [ "$?" = "0" ] ; then
    echo "ok!"
  else
    echo "error!"
    >&2 echo "<!!> Failed to unzip openssl archive. See $_TMP/openssl-$_VERSION-config.log"
    exit 1
  fi

  printf "<**> Configure openssl $_VERSION."
  case $(uname | tr '[:upper:]' '[:lower:]') in
    linux* | msys*)
      EXTRA_OSSL_CONFIG=-Wl,-rpath="${_PREFIX}/lib"
      ;;
  esac
  cd "$_OPENSSL_PATH"
  ./config --prefix="$_PREFIX" --openssldir="$_PREFIX" $EXTRA_OSSL_CONFIG 2>&1 1>> "$_TMP/openssl-$_VERSION-config.log"
  if [ "$?" = "0" ] ; then
    echo "ok!"
  else
    echo "error!"
    >&2 echo "<!!> Failed to configure openssl $_VERSION. See $_TMP/openssl-$_VERSION-config.log"
    exit 1
  fi

  printf "<**> Build openssl $_VERSION."
  make 2>&1 1>> "$_TMP/openssl-$_VERSION-config.log"
  if [ "$?" = "0" ] ; then
    echo "ok!"
  else
    echo "error!"
    >&2 echo "<!!> Failed to compile openssl $_VERSION. See $_TMP/openssl-$_VERSION-config.log"
    exit 1
  fi

  printf "<**> Install openssl $_VERSION in $_PREFIX."
  make install 2>&1 1>> "$_TMP/openssl-$_VERSION-config.log"
  if [ "$?" = "0" ] ; then
    echo "ok!"
  else
    echo "error!"
    >&2 echo "<!!> Failed to install openssl $_VERSION. See $_TMP/openssl-$_VERSION-config.log"
    exit 1
  fi

  printf "<**> Remove temporary files."
  cd "$_CURRENT_DIR"
  rm -rf "$_TMP"
  echo "ok!"

  if [ "$_CONFIGURE" = "yes" ] ; then
    printf "<**> Set config OPENSSL to $_OPENSSL_EXE."
    set_config OPENSSL "$_OPENSSL_EXE"
    if [ "$?" = "0" ] ; then
      echo "ok!"
    else
      echo "error!"
      >&2 echo "<!!> Failed to configure ${CLI_MAIN_COMMAND}."
      exit 1
    fi
  fi

  exit 0
}
