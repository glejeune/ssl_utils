#!/bin/sh

_PORT=443
_ONLY=""
_NB=0
while [ "$1" ] ; do
  case "$1" in
    "--port" | "-p" )
      shift
      _PORT="$1"
      shift ;;
    "--host" | "-H" )
      shift
      _SERVERNAME="$1"
      shift ;;
    "--help" | "-h" )
      __help --plugin "$PLUGIN_NAME" --exit 0
      shift ;;
    "--only" | "-o" )
      shift ;
      _ONLY=$(echo "$1" | sed -e 's/,/ /g')
      shift ;;
    "--")
      shift ;;
    -*)
      __help --plugin "$PLUGIN_NAME" --exit 1 --msg "$1: Invalid option"
      shift ;;
    *)
      eval "FILE_OR_DOMAIN_$_NB=$1"
      _NB=$((_NB + 1))
      shift;;
  esac
done

if [ "$_NB" = "0" ] ; then
  __help --msg "missing file or domain" --plugin "$PLUGIN_NAME" --exit 1
fi

_N=0
while [ "$_N" -lt "$_NB" ] ; do
  if [ "$_N" -gt "0" ] ; then
    echo
  fi
  eval "FILE_OR_DOMAIN=\${FILE_OR_DOMAIN_$_N}"
  if [ -f "$FILE_OR_DOMAIN" ] ; then
    file_info "$FILE_OR_DOMAIN"
  else
    domain_info "$FILE_OR_DOMAIN" "$_PORT" "$_SERVERNAME"
  fi
  _N=$((_N + 1))
done
