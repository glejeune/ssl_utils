#!/bin/sh

_PORT=443
while [ "$1" ] ; do
  case "$1" in
    "--port" | "-p" )
      shift
      _PORT="$1"
      shift ;;
    "--host" | "-H" )
      shift
      _SERVERNAME="$1"
      shift ;;
    "--help" | "-h" )
      __help --plugin "$PLUGIN_NAME" --exit 0
      shift ;;
    "--")
      shift ;;
    -*)
      __help --plugin "$PLUGIN_NAME" --exit 1 --msg "$1: Invalid option"
      shift ;;
    build)
      shift
      build $@
      exit 0 ;;
    *)
      _DOMAIN="$1"
      shift;;
  esac
done

display "$_DOMAIN" "$_PORT" "$_SERVERNAME"
