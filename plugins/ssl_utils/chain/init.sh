#!/bin/sh

PLUGIN_VERSION="0.0.1"
PLUGIN_DESCRIPTION="Get domain chain"
PLUGIN_AUTHORS="Greg"

display() {
  _DOMAIN="$1"
  _PORT="$2"
  _SERVERNAME="$3"
  _SERVERNAME_CMD=""

  if [ "z$_DOMAIN" = "z" ] ; then
    __help --msg "missing domain" --plugin "$PLUGIN_NAME" --exit 1
  fi

  if [ "x" = "x$_SERVERNAME" ] ; then
    _SERVERNAME_CMD="-servername $_DOMAIN"
  else
    _SERVERNAME_CMD="-servername $_DOMAIN"
    _DOMAIN="$_SERVERNAME"
  fi

  if [ "$IS_LIBRESSL" = "0" ] ; then
    _TMP_CHAIN=$(mktemp)
    _TMP_CHAIN_FILE_BASE=$(mktemp -u)

    "$OPENSSL" s_client -showcerts $_SERVERNAME_CMD -verify 5 -connect "$_DOMAIN:$_PORT" 2>/dev/null < /dev/null | sed -n '/-----BEGIN/,/-----END/p' > "$_TMP_CHAIN"
    gawk "/-----BEGIN/{if(NR!=1){for(i=0;i<j;i++)print a[i]>\"${_TMP_CHAIN_FILE_BASE}_\"k\".crt\";j=0;k++;}a[j++]=\$0;next}{a[j++]=\$0;}END{for(i=0;i<j;i++)print a[i]>\"${_TMP_CHAIN_FILE_BASE}_\"k\".crt\"}" i=0 k=1 "$_TMP_CHAIN"

    _CERT=$(find "$(dirname "$_TMP_CHAIN_FILE_BASE")" -name "$(basename "$_TMP_CHAIN_FILE_BASE")*" 2>/dev/null | sort | head -1)
    _CHAIN=$(find "$(dirname "$_TMP_CHAIN_FILE_BASE")" -name "$(basename "$_TMP_CHAIN_FILE_BASE")*" 2>/dev/null | sort | tail +2 | xargs)

    cat $_CHAIN > "$_TMP_CHAIN"

    "$OPENSSL" verify -show_chain -untrusted "$_TMP_CHAIN" "$_CERT" 2>/dev/null | grep -i depth | cut -d= -f2-

    rm -f "$_TMP_CHAIN"
    rm -f "${_TMP_CHAIN_FILE_BASE}"_*
  else
    "$OPENSSL" s_client -connect "$_DOMAIN:$_PORT" -verify 5 $_SERVERNAME_CMD 2>&1 </dev/null | grep depth= | sort | sed -e 's/^depth=\([0-9]*\) */\1: /'
  fi
}

set_file_by_type() {
  __FILE="$(expand "$(dirname "$1")")/$(basename "$1")"
  __TYPE=$(cat "$__FILE" | grep "\\-*BEGIN" | head -1 | sed -e 's/^-*BEGIN *\([^-]*\)-*.*$/\1/' | tr "[A-Z]" "[a-z]")
  case "$__TYPE" in
    "certificate")
      __CERT=$__FILE
      ;;
    "rsa private key")
      __KEY=$__FILE
      ;;
    "private key")
      __KEY=$__FILE
      ;;
    *)
      __help --msg "invalid file" --plugin "$PLUGIN_NAME" --exit 1
      ;;
  esac
}

build() {
  _OUTPUT="."
  _CA_CERTIFICATES_PATH=/usr/share/ca-certificates/mozilla

  while [ "$1" ] ; do
    case "$1" in
      "--output" | "-o" )
        shift
        _OUTPUT="$1"
        shift ;;
      "--ca-cert-dir" | "-c" )
        shift
        _CA_CERTIFICATES_PATH="$1"
        shift ;;
      *)
        set_file_by_type "$1"
        shift ;;
    esac
  done

  if [ "z$__CERT" = "z" ] ; then
    __help --msg "missing certificate" --plugin "$PLUGIN_NAME" --exit 1
  fi

  if [ "z$__KEY" = "z" ] ; then
    __help --msg "missing key" --plugin "$PLUGIN_NAME" --exit 1
  fi

  # Download ICA Cert
  _CA_ISSUERS_URL=$(openssl x509 -in ${__CERT} -text -noout | grep "CA Issuers - URI:" | sed -e 's/^\s*CA Issuers - URI://')
  curl --output CA_Issuers_Cert.crt ${_CA_ISSUERS_URL} 2>/dev/null
  _CA_ISSUERS_CN=$(openssl x509 -in CA_Issuers_Cert.crt -inform der -subject -noout | sed -e 's/^.*CN\s*=\s*\([^,]*\)/\1/' | sed -e 's/\s\+/_/g')
  _CA_ISSUERS_SERIAL=$(openssl x509 -in CA_Issuers_Cert.crt -inform der -serial -noout | sed -e 's/serial\s*=\s*//')
  _CA_ISSUERS_FILE=${_CA_ISSUERS_CN}_${_CA_ISSUERS_SERIAL}.crt
  openssl x509 -in CA_Issuers_Cert.crt -inform der -out ${_CA_ISSUERS_FILE}
  rm -f CA_Issuers_Cert.crt

  # Add ICA to cert
  _CERT_CN=$(openssl x509 -in ${__CERT} -noout -subject | sed -e 's/^.*CN\s*=\s*\([^,]*\)/\1/' | sed -e 's/\.\+/_/g' | sed -e 's/\*/wildcard/')
  _CERT_SERIAL=$(openssl x509 -in ${__CERT} -noout -serial | sed -e 's/serial\s*=\s*//')
  _CERT_FILE_NAME=${_CERT_CN}_${_CERT_SERIAL}.crt
  cat ${__CERT} ${_CA_ISSUERS_FILE} > tmp
  mv tmp ${_CERT_FILE_NAME}

  # Copy key file
  _KEY_FILE_NAME=${_CERT_CN}_${_CERT_SERIAL}.key
  cp ${__KEY} ${_KEY_FILE_NAME}

  # Find CA Cert
  _CA_HASH=$(openssl x509 -in ${_CA_ISSUERS_FILE} -issuer_hash -noout)
  for _CA_FILE in `find ${_CA_CERTIFICATES_PATH} -type f` ; do
    _TMP_CA_HASH=$(openssl x509 -in ${_CA_FILE} -hash -noout)
    if [ "${_CA_HASH}" = "${_TMP_CA_HASH}" ] ; then
      _CA_ROOT_FILE=${_CA_FILE}
      break
    fi
  done
  cat ${_CA_ROOT_FILE=} ${_CA_ISSUERS_FILE} > tmp
  mv tmp ${_CA_ISSUERS_FILE}

  # Move to final directory
  if [ ! "$_OUTPUT" = "." ] ; then
    mkdir -p ${_OUTPUT}
    mv ${_CERT_FILE_NAME} ${_OUTPUT}
    mv ${_KEY_FILE_NAME} ${_OUTPUT}
    mv ${_CA_ISSUERS_FILE} ${_OUTPUT}
  fi

  echo "ssl_certificate         ${_OUTPUT}/${_CERT_FILE_NAME}"
  echo "ssl_certificate_key     ${_OUTPUT}/${_KEY_FILE_NAME}"
  echo "ssl_trusted_certificate ${_OUTPUT}/${_CA_ISSUERS_FILE}"
}
