#!/bin/sh
# DO NOT CHANGE THIS FILE
# IF YOU WANT TO CONFIGURE ssl_utils PLEASE COPY THIS FILE TO
#   $HOME/.config/ssl_utils/config
# AND UPDATE IT.
OCSPCHECK=$(which ocspcheck)
OPENSSL=$(which openssl)
